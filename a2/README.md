> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Jimmy Anderson

### Assignment 2 Requirements:

*Three Parts:*

1. Create Healthy Recipes Andorid App
2. Complete Java skill sets (1-3)
3. Chapter Questions (Chp. 3 - 4)

#### README.md file should include the following items:

* Screenshot of Healthy Recipes app: splash page
* Screenshot of Healthy Recipes app: recipe page
* Screenshot of Java Skill set 1
* Screenshot of Java Skill set 2
* Screenshot of Java Skill set 3

#### Assignment Screenshots:

| *Screenshot of Healthy Recipes App - Splash Page*: | *Screenshot of Healthy Recipes App - Recipe Page*: |
| :----:        |    :----:   |
| ![Healthy Recipes App Screenshot - Splash Page](img/bruschetta1.png) | ![Healthy Recipes App Screenshot - Recipe Page](img/bruschetta2.png) |

| *Screenshot of Java skill set one - Even or Odd*: | *Screenshot of Java skill set two - Largest Number*: | *Screenshot of Java skill set three - Arrays and Loops*: |
| :----:        |    :----:   |   :----:    |
| ![Java Skill Set One](img/evenorodd.png) | ![Java Skill Set Two](img/largestnumber.png) | ![Java Skill Set Three](img/arraysandloops.png) |

#### Tutorial Links:

*Android Tutorial - Healthy Recipes:*
[Healthy Recipes Tutorial](http://www.qcitr.com/vids/LIS4381_A2.mp4 "Bitbucket Station Locations")
