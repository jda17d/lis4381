> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Jimmy Anderson

### Assignment 3 Requirements:

*Four Parts:*

1. Create a database for petstores
2. Create Android Application that calculates prices for tickets
3. Chapter Questions (Chp. 5 - 6)
4. Skill set 4-6

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of my event Android application running splash screen and results
* Screenshots of the completed Java skill sets
* Links to the A3.mwb and A3.sql files

#### Assignment Screenshots:

| *Screenshot of petstore ERD*: |
| :----:  |
| ![Petstore ERD](img/A3.png) |

| *Screenshot of My Event - Splash Page*: | *Screenshot of My Event - Results*: |
| :----:        |    :----:   |
| ![My Event App Screenshot - Splash Page](img/myevent1.png) | ![My Event App Screenshot - Results](img/myevent2.png) |

| *Screenshot of Java skill set four - Decision Structures*: | *Screenshot of Java skill set Five - Random Generator*: | *Screenshot of Java skill set Six - Methods*: |
| :----:        |    :----:   |   :----:    |
| ![Java Skill Set Four](img/ss4.png) | ![Java Skill Set Five](img/ss5.png) | ![Java Skill Set Six](img/ss6.png) |

#### Document Links:

*A3 MWB file:*
[A3 MWB file](https://bitbucket.org/jda17d/lis4381/raw/39c0d34380bf14c1b060e4afbd65410695665f80/a3/docs/A3.mwb "A3 MWB file")

*A3 SQL file:*
[A3 SQL file](https://bitbucket.org/jda17d/lis4381/src/39c0d34380bf14c1b060e4afbd65410695665f80/a3/docs/A3.sql "A3 SQL file")