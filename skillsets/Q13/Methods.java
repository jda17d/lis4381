import java.util.Scanner;
import java.io.*;

public class Methods
{
    //nonvalue-returning method (static requires no object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Sphere Volume Calculator\n");
        System.out.println("Developer: Jimmy Anderson");
        System.out.println("Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches, and rounds to two decimal places.");
        System.out.println("Must use Java's *built-in* PI and pow() capabilities");
        System.out.println("Program checks for non-integers and non-numeric values.");
        System.out.println("Programs continues to prompt for user entry until no longer requested, prompt accepts upper or lower case letters.");
        System.out.println(); //print blank line
    }

    //nonvalue-returning method (static requires no object)
    public static void getVolume()
    {
        final Scanner input = new Scanner(System.in);
        String response = "y";
        int num = 0;

        while (response.equals("y")){
            System.out.print("Please enter diameter in inches: ");
            while (!input.hasNextInt()){
                System.out.println("Not valid integer!");
                input.next();
                System.out.print("\nPlease try again. Enter diameter in inches: ");
            }
            num = input.nextInt();

            System.out.println("\nSphere volume: " + doConversion(num) + " liquid U.S. gallons");
            System.out.print("\nDo you want to calculate another sphere volume (y or n)?");
            response = input.next().toLowerCase();
            input.nextLine();
        }

        System.out.println("\nThank you for using our Sphere Volume Calculator");
    }

    public static double calculateVolume(int diameter){

        //volume of a sphere: v=(4/5)(pi)((1/2)d)^3
        //Math.PI()
        //Math.pow()
        double volume = 0.0;
        int radius = diameter /2;
        double r3 = Math.pow(radius, 3);
        volume = (4.0/3.0) * (Math.PI) * r3;
        return volume;
    }

    public static String doConversion(int diameter){
        //US gallons = volume * 0.00432900433
        double volume = calculateVolume(diameter);
        double conversion = 0.00432900433;
        double gallons = volume * conversion;
        String myStr = String.format("%.2f", gallons);
        return myStr;
    }
}