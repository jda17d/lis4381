> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Jimmy Anderson

### Assignment 4 Requirements:

*Five Parts:*

1. Create Bootstrap carousel to promote different projects
2. Create a form to collect data and complete bootstrap client-side validation
3. Provide link to local LIS4381 Web App
4. Chapter Questions (Chp. 9, 10, and 15)
5. Skill set 10-12

#### README.md file should include the following items:

* Link to local LIS4381 Web App
* Screenshot of Homepage / Carousel
* Screenshot of Assignment 4 form with invalid data
* Screenshots of Assignment 4 form with valid data

#### Assignment Links:

*Local Host LIS4381 Web App:*
[Local Host LIS4381 Web App](http://localhost/repos/classes/lis4381/index.php "LIS4381 Local Host")

#### Assignment Screenshots:

| *Screenshot of Homepage / Carousel*: | *Screenshot of Assignment 4 with invalid data*: | *Screenshot of Assignment 4 with valid data*: |
| :----:        |    :----:   |   :----:    |
| ![Homepage](img/homepage.png) | ![Failed validation](img/failed.png) | ![Passed validation](img/passed.png) |

| *Screenshot of Java skill set Ten - Array lists*: | *Screenshot of Java skill set Eleven - Nested Structures*: | *Screenshot of Java skill set Twelve - Temperature Conversion Program*: |
| :----:        |    :----:   |   :----:    |
| ![Java Skill Set Ten](img/ss10.png) | ![Java Skill Set Eleven](img/ss11.png) | ![Java Skill Set Twelve](img/ss12.png) |

#### Tutorial Links:

*A4 Part 1:*
[A4 Part 1 Tutorial](http://www.qcitr.com/vids/LIS4381_A4a.mp4 "A4 Tutorial Part 1")

*A4 Part 2:*
[A4 Part 2 Tutorial](http://www.qcitr.com/vids/LIS4381_A4b.mp4 "A4 Tutorial Part 2")