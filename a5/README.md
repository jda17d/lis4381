> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Jimmy Anderson

### Assignment 5 Requirements:

*Four Parts:*

1. Developed server-side validation for form data
2. Created a form to insert data into the database
3. Chapter Questions (Chp. 11, 12, & 19)
4. Skill set 13-15

#### README.md file should include the following items:

* Link to local host web application
* Screenshot of A5 index with included data from the database
* Screenshot of server-side validation error
* Screenshots of Java skill sets

#### Assignment Links:

*Local Host LIS4381 Web App:*
[Local Host LIS4381 Web App](http://localhost/repos/classes/lis4381/index.php "LIS4381 Local Host")

#### Assignment Screenshots:

| *Screenshot of Index.php (populated)*: | *Screenshot of server-side validation (error)*: |
| :----:        |    :----:   |
| ![Homepage](img/index.png) | ![Failed validation](img/error.png) |

| *Screenshot of Java skill set Thirteen - Sphere Volume Calc*: |
| :----:        |
| ![Java Skill Set Thirteen](img/ss13.png) |

| *Screenshot of Java skill set Fourteen - PHP Simple Calc-Add*: | *Screenshot of skill set Fourteen - PHP Simple Calc-Add Result*: |
| :----:        |    :----:   |
| ![Java Skill Set Fourteen](img/ss14p1.png) | ![Java Skill Set Fourteen](img/ss14p2.png) |

| *Screenshot of Java skill set Fourteen - PHP Simple Calc-Divide*: | *Screenshot of skill set Fourteen - PHP Simple Calc-Divide Error*: |
| :----:        |    :----:   |
| ![Java Skill Set Fourteen](img/ss14p3.png) | ![Java Skill Set Fourteen](img/ss14p4.png) |

| *Screenshot of Java skill set Fifteen - PHP Write/Read File*: | *Screenshot of skill set Fifteen - PHP Write/Read File-Result*: |
| :----:        |    :----:   |
| ![Java Skill Set Fifteen](img/ss15p1.png) | ![Java Skill Set Fifteen](img/ss15p2.png) |

#### Tutorial Links:

*A5 Part 1:*
[A5 Part 1 Tutorial](http://www.qcitr.com/vids/LIS4381_A5a.mp4 "A5 Tutorial Part 1")

*A5 Part 2:*
[A5 Part 2 Tutorial](http://www.qcitr.com/vids/LIS4381_A5b.mp4 "A5 Tutorial Part 2")

*A5 Part 3:*
[A5 Part 3 Tutorial](http://www.qcitr.com/vids/LIS4381_A5c.mp4 "A5 Tutorial Part 3")