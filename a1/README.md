> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Jimmy Anderson

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chp. 1 - 2)

#### README.md file should include the following items:

* Screenshot of AMPPS installation.
* Screenshot of running java Hello.
* Screenshot of running Android Studio - My First App.
* git commands w/short descriptions.
* Bitbucket repo links. a) this assignment and b) the completed tutorials above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create a new local repository.
2. git status - List the files you've changed and those you still need to add or commit.
3. git add - Add one or more files to staging (index).
4. git commit - Commit any files youve added with git add, and also commit any files you've cahnged since then.
5. git push - Send changes to the master branch of your remote repository.
6. git pull - Fetch and merge changes on the remote server to your working directory.
7. git clone - Create a working copy of a local/remote repository.

#### Assignment Screenshots:

| *Screenshot of AMPPS running http://localhost* : |
|     :----:    |
| ![AMPPS Installation Screenshot](img/ampps.png) |

| *Screenshot of running java Hello*: |
|     :----:    |
| ![JDK Installation Screenshot](img/jdk_install.png) |

| *Screenshot of Android Studio - My First App - Pixel 3*: | *Screenshot of Android Studio - My First App - Nexus 5*: |
| :----:        |    :----:   |
| ![Android Studio Installation Screenshot - My First App - Pixel 3](img/helloworldpixel3.png) | ![Android Studio Installation Screenshot - My First App - Nexus 5](img/helloworldnexus5.png) |

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jda17d/bitbucketstationlocations/ "Bitbucket Station Locations")
