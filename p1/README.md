> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Jimmy Anderson

### Project 1 Requirements:

*Three Parts:*

1. Create Business Card Application
2. Complete Java skill sets (7-9)
3. Chapter Questions (Chp. 7 - 8)

#### README.md file should include the following items:

* Screenshot of Business Card app: splash page
* Screenshot of Business Card app: info page
* Screenshot of Java Skill set 7
* Screenshot of Java Skill set 8
* Screenshot of Java Skill set 9

#### Assignment Screenshots:

| *Screenshot of Business Card App - Splash Page*: | *Screenshot of Business Card App - Info Page*: |
| :----:        |    :----:   |
| ![Business Card App Screenshot - Splash Page](img/businesscard1.png) | ![Business Card App Screenshot - Info Page](img/businesscard2.png) |

| *Screenshot of Java skill set seven - Random Array Using Methods and Data Validation*: | *Screenshot of Java skill set eight - Largest Three Numbers*: | *Screenshot of Java skill set nine - Array Runtime Data Validation*: |
| :----:        |    :----:   |   :----:    |
| ![Java Skill Set One](img/ss7.png) | ![Java Skill Set Two](img/ss8.png) | ![Java Skill Set Three](img/ss9.png) |
