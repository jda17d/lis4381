> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Jimmy Anderson

### Project 2 Requirements:

*Four Parts:*

1. Add Edit/Delete functionality to A5
2. Finalize Carousel Homepage
3. Added RSS Feed
4. Chapter Questions (Chp. 13, 14)

#### README.md file should include the following items:

* Link to local host web application
* Screenshot of P2 Index
* Screenshot of P2 Edit/Update page
* Screenshot of P2 Error page
* Screenshot of P2 Carousel
* Screenshot of RSS feed

#### Assignment Links:

*Local Host LIS4381 Web App:*
[Local Host LIS4381 Web App](http://localhost/repos/classes/lis4381/index.php "LIS4381 Local Host")

#### Assignment Screenshots:

| *Screenshot of Index page (populated)*: | *Screenshot of Edit page*: |
| :----:        |    :----:   |
| ![Index Page](img/p2index.png) | ![Edit Page](img/p2update.png) |

| *Screenshot of Error page (populated)*: | *Screenshot of Homepage (Carousel)*: |
| :----:        |    :----:   |
| ![Index Page](img/p2error.png) | ![Homepage Carousel](img/homepage.png) |

| *Screenshot of RSS Feed (BBC News US/Canada)*: |
| :----:        |
| ![RSS Feed](img/rss.png) |

#### Tutorial Links:

*P2 Part 1:*
[P2 Part 1 Tutorial](http://www.qcitr.com/vids/LIS4381_P2_a.mp4 "P2 Tutorial Part 1")

*P2 Part 2:*
[P2 Part 2 Tutorial](http://www.qcitr.com/vids/LIS4381_P2_b.mp4 "P2 Tutorial Part 2")

*RSS Feed Part 3:*
[RSS Feed Tutorial](http://www.qcitr.com/vids/LIS4381_P2_RSS_Feed.mp4 "RSS Feed Tutorial Part")