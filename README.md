> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
# LIS4381 - Mobile Web Application Development

## Jimmy Anderson

### LIS4381 Requirements:

*Course Work Links:*

#### Assignments

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Installed AMPPS
    * Installed JDK
    * Installed Android Studio and Create My First App
    * Provided screenshots of installations
    * Created Bitbucket repo
    * Completed Bitbucket tutorials (bitbucketstationlocations)
    * Provided git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Created Healthy Recipes Android app
    * Provided screenshots of completed app
    * Provided screenshots of completed Java skill sets 1-3
3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Created ERD of petstore, pet, and customer database
    * Successfuly foward engineered the database and populated tables with 10 records
    * Created my event application in Android Studio to calculate prices of concert tickets
    * Provided screenshots of the ERD and my event application screens
    * Provided screenshots of completed Java skill sets 4-6
4. [A4 README.md](a4/README.md "My A4 README.md file")
    * Create Bootstrap carousel to promote different projects
    * Create a form to collect data and complete bootstrap client-side validation
    * Provided screenshots of carousel and form validation
    * Provided screenshots of complete Java skill set 10-12
5. [A5 README.md](a5/README.md "My A5 README.md file")
    * Developed server-side validation for form data
    * Created a form that inserts data into database
    * Provided screenshots of completed Java skill set 13-15

#### Projects

6. [P1 README.md](p1/README.md "My A6 README.md file")
    * Backwards engineered and customized Business Card application
    * Provided screenshots of Business Card application
    * Provided screenshots of completed Java skill sets 7-9
7. [P2 README.md](p2/README.md "My A7 README.md file")
    * Implemented Update/Edit and Delete functionality to A5
    * Finalized Homepage Carousel
    * Implemented RSS feed in the Test directory
    * Provided screenshots of index/edit/error/carousel/rss feed